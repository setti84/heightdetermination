const fs = require('fs');
const RBush = require('rbush');
const Bounds = require('./../Bounds.js');
const proj4 = require('proj4');
const util = require('./../util.js');

const config = require('./../config.json');
const Logger = require('./../Logger.js');
const log = new Logger('./log/log.txt', true , config.logInFile);

module.exports = class Preprocessor {

  constructor(getPath = './data/downloaded/', savePath = './data/preprocessed/', jobs = []){

    this.jobs = jobs;
    this.getPath = getPath;
    this.savePath = savePath;

    if (!fs.existsSync(this.getPath)){
      log.app(Error(`Could not find folder for Preprocessing ${this.getPath}`));
    }

    if (!fs.existsSync(this.savePath)){
      fs.mkdirSync(this.savePath);
    }

  }

  getJobs(){

    return new Promise((resolve) =>{

      fs.readdir(this.getPath, (err, files) => {
        files.forEach( file => {
          if(file.substring(0,3) === 'OSM'){

            const filename = file.substring(0,file.length-5);
            const parts = filename.split("_");

            this.jobs.push({
              x: parts[1],
              y: parts[2],
              osm: file,
              dtm: `DTM_${file.substring(4,file.length)}`,
              dsm: `DSM_${file.substring(4,file.length)}`
            });
          }
        });
        log.app(`Jobs for Preprocessing: ${this.jobs.length}`);
        resolve();
      });
    });

  }

  async process(){

    // return;

      while (this.jobs.length > 0) {

        const job = this.jobs.shift();

        log.app(`Job: ${job.x}_${job.y}`);

        if(fs.existsSync(`${this.savePath}OSM_${job.x}_${job.y}.json`) &&
           fs.existsSync(`${this.savePath}diffDOM_${job.x}_${job.y}.json`)
        ){
          log.app(`Data for ${job.x}_${job.y} already preprocessed`)
          continue;
        }

        const treeOSM = new RBush();

        try{

          // const t0 = new Date();

          const data = await this.readFiles(job);
          // console.log("time " + (new Date() - t0) + " for read files");

          // // const t1 = new Date();
          const osm = JSON.parse(data[0]);
          const dtm = JSON.parse(data[1]);
          const dsm = JSON.parse(data[2]);

          // console.log("time " + (new Date() - t1) + " for parsing");

          // const t2 = new Date();
          treeOSM.load(this.ReqArea(osm.features));
          const tempDTM = this.clipOnOSM( treeOSM , dtm);
          const tempDSM = this.clipOnOSM( treeOSM , dsm);
          log.app(`DTM-size: ${tempDTM.length} DSM-size: ${tempDSM.length}`)
          // console.log("time " + (new Date() - t2) + " for clipping on osm");

          // const t3 = new Date();
          const nDOM = this.createNDOM(tempDSM, tempDTM);
          log.app(`Diff DOM-size: ${nDOM.length}`);
          // console.log("time " + (new Date() - t3) + " for creating difference DOM");

          log.app(`save diffDOM: ${job.osm.substr(3,job.osm.length)}`);
          this.saveFile(`diffDOM${job.osm.substr(3,job.osm.length)}`, nDOM);
          fs.writeFileSync(`${this.savePath}${job.osm}`, JSON.stringify(osm));

        } catch(err){
          log.app(`Cant find file: ${err}`);
          // this.deleteFiles(job);

        }

      }

  }

  createNDOM(tempDSM, tempDTM){

    // sometimes it happens that DSM and DTM don't have the same area covered by height points.
    // This is a problem of the provided data by the local authority. In this case the DTM is put in
    // a spatial tree for easier finding the corresponding DSM points. After that process the different height is calculated

    const tree = new RBush();
    tree.load(this.prepareForTree(tempDTM));

    return tempDSM.reduce( (filtered, element) => {

      const result = tree.search({ minX: element.x, minY: element.y, maxX: element.x, maxY: element.y });

      if(result.length === 1){
        filtered.push({x: element.x, y: element.y, z: Math.round( (element.z - result[0].feature.z) * 100) / 100 });
      }

      return filtered;

    }, []);

  }

  clipOnOSM(treeOSM, surfaceModel) {

      return surfaceModel.filter( item => {
        return treeOSM.collides({minX: item.x, minY: item.y, maxX: item.x, maxY: item.y});
      });

  }

  ReqArea(osm) {
    return osm.map( feature => {
      const boundsLatLng = Bounds.fromGeometry(feature.geometry);
      const boundsMin = proj4(util.LONGLATPROJ, util.UTMPROJWORLD, [boundsLatLng.min[0], boundsLatLng.min[1]]);
      const boundsMax = proj4(util.LONGLATPROJ, util.UTMPROJWORLD, [boundsLatLng.max[0], boundsLatLng.max[1]]);

      return {
        minX: parseInt(boundsMin[0]),
        minY: parseInt(boundsMin[1]),
        maxX: parseInt(boundsMax[0]),
        maxY: parseInt(boundsMax[1]),
        feature: feature
      };
    });
  }

  prepareForTree(surfaceModel){

  return surfaceModel.map(( point)=>{
    return {
      minX: point.x,
      minY: point.y,
      maxX: point.x,
      maxY: point.y,
      feature: {x: point.x, y: point.y, z: point.z}
    };
  });

}

  readFiles(job){

    const osm = new Promise((resolve, reject) => {

      fs.readFile(`${this.getPath}${job.osm}`, (err, data) => {

        if (err){
          reject(err);
        } else{
          resolve(data);
        }

      });

    });

    const dtm = new Promise((resolve, reject) => {

      fs.readFile(`${this.getPath}${job.dtm}`, (err, data) => {

        if (err){
          reject(err);
        } else{
          resolve(data);
        }

      });

    });

    const dsm = new Promise((resolve, reject) => {

      fs.readFile(`${this.getPath}${job.dsm}`, (err, data) => {

        if (err){
          reject(err);
        } else{
          resolve(data);
        }

      });

    });

    return Promise.all([osm, dtm, dsm]);
  }

  saveFile(name ,data){

    fs.writeFileSync(`${this.savePath}${name}`, JSON.stringify(data));

  }

  deleteFiles(job){

    // Should not be deleted we need it for later
    // try{
    //   fs.unlinkSync(`${this.getPath}${job.osm}`);
    // }
    // catch(err){
    //   log.app(err)
    // }
    try{
      fs.unlinkSync(`${this.getPath}${job.dtm}`);
    }
    catch(err){
      log.app(err)
    }
    try{
      fs.unlinkSync(`${this.getPath}${job.dsm}`);
    }
    catch(err){
      log.app(err)
    }

  }

};



