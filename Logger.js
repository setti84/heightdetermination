const fs = require('fs');

module.exports = class Logger{

  constructor(path = './', cons = true, file = false){

    this.path = path;
    this.dateStart = new Date().toISOString();
    this.cons = cons;
    this.file = file;

    if(file && !fs.existsSync(path)){
      try{
        fs.writeFileSync( `${this.path}` , `Start Script: ${this.dateStart} \n`, { flag: 'a' } );
      }catch(err){
        console.log(err)
      }
    }

  }

  app(text){

    if(this.file){
      fs.appendFile( `${this.path}` ,`${new Date().toISOString()}: ${text} \n`, 'utf8', err => {
        if (err) throw err;
      });
    }
    if(this.cons){
      console.log(text)
    }
  }

};

/*
usage:

const Logger = require('./Logger.js');
const log = new Logger('./log/', true ,false); // path where to save log file , write to console, write to file

usage: log.app(text)

 */