
const longlatProj = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs ';

// EPSG 25833
const utmProjBer = '+proj=utm +zone=33 +ellps=GRS80 +units=m +no_defs';
// utm zone berlin epsg/32633
// const utmProjBer = '+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs';

// EPSG: 3857
const utmProjWorld = '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs';

// https://epsg.io/3395 :  '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'

const pointInPolygon = (point, polygon) => {
  let x = point[0], y = point[1], inside = false;
  for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
    let xi = polygon[i][0], yi = polygon[i][1];
    let xj = polygon[j][0], yj = polygon[j][1];
    let intersect = ((yi > y) !== (yj > y)) &&
      (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
    if (intersect) inside = !inside;
  }
  return inside;
};

module.exports = {
  LONGLATPROJ: longlatProj,
  UTMPROJBER: utmProjBer,
  UTMPROJWORLD: utmProjWorld,
  pointInPolygon: pointInPolygon
};