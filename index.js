

const Logger = require('./Logger.js');

const config = require('./config.json');

const Reader = require('./reader/Reader.js');
const Preprocessor = require('./preprocessing/Preprocessor.js');
const Heightprocessor = require('./heightprocessing/Heightprocessor.js');

const log = new Logger('./log/log.txt', true, config.logInFile);

const read = new Reader(
  config.bbox,
  config.tilesize,
  config.osm,
  config.dsm,
  config.dtm,
  config.saveDownload
);

const preprocessing = new Preprocessor(config.saveDownload, config.saveDiffDOM, []);

const heightprocessing = new Heightprocessor();


// min in UTM: east: 370000 north: 5798000
// min in WGS84:  13.092766, 52.316916

// max in UTM: east: 414000 north: 5836000
// max in WGS84:   13.728261, 52.667117

let t5 = new Date();

read.download()
  .then( ()=>{

  log.app( '++++++++++ Done with downloads. Start preprocess-getJobs ++++++++++' );
  return preprocessing.getJobs('./data/downloaded/');

})
  .then( () =>{

  log.app( '++++++++++ Done with preprocess-getJobs. Start preprocess-process ++++++++++' );
  return preprocessing.process();

})
  .then( () =>{

  log.app( '++++++++++ Done with preprocess-process. Start with height processing ++++++++++' );
  return heightprocessing.getJobs();

})
  .then( () =>{

  // log.app(heightprocessing.jobs);

  heightprocessing.process();
  console.log("time " + (new Date() - t5) + " for everything");
  log.app( '++++++++++ Done with heightprocessing. Finished! ++++++++++' );

});