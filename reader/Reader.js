
// const https = require('https');
const fs = require('fs');
const proj4 = require('proj4');
const util = require('./../util.js');
const osmtogeojson = require('osmtogeojson');
const RBush = require('rbush');

const AdmZip = require('adm-zip');
const request = require('request');

const Bounds = require('./../Bounds.js');
const Logger = require('./../Logger.js');
const config = require('./../config.json');
const log = new Logger('./log/log.txt', true , config.logInFile);

module.exports = class Reader{

  constructor(bbox = [], tilesize = 2000, osm = '', dsm = '', dtm = '', savePath = 'data/downloaded/'){

    this.bbox = bbox;
    this.tilesize = tilesize;
    this.osm = osm;
    this.dsm = dsm;
    this.dtm = dtm;
    this.jobs = this.createJobs();
    this.savePath = savePath;

  }

  createJobs() {

    log.app('create Jobs from Bbox');

    const jobs = [];
    let x = 0, y = 0;

    for(let i = this.bbox[1]; i<this.bbox[3]; i+=this.tilesize){
      y++;
      x = 0;
      for(let j = this.bbox[0]; j<this.bbox[2]; j+=this.tilesize){
        x++;

        let min = proj4(util.UTMPROJBER, util.LONGLATPROJ, [j, i]);
        min = [ this.round(min[0]) , this.round(min[1])];

        let max = proj4(util.UTMPROJBER, util.LONGLATPROJ, [j+this.tilesize, i+this.tilesize]);
        max = [this.round(max[0]), this.round(max[1])];

        const job = {
          'x': x,
          'y': y,
          'tileX': j,
          'tileY': i,
          'dtm': this.dtm,
          'dsm': this.dsm,
          'osm': `${this.osm}[out:json][timeout:30];( way["building"](${min[1]},${min[0]},${max[1]},${max[0]}); relation["building"]["type"="multipolygon"](${min[1]},${min[0]},${max[1]},${max[0]}); );out;>;out qt;`
        };

        jobs.push(job);

        // }
      }
    }

    log.app(`Jobs for Download: ${jobs.length}`);

    // return [jobs[1], jobs[100]];
    return [jobs[270], jobs[271], jobs[100], jobs[1]];
    // return [jobs[100]];
    // return jobs;

  }

  async download(){

    // return;

    while (this.jobs.length > 0) {

      const job = this.jobs.shift();

      const treeOSM = new RBush();
      let osm;

      log.app(`Open jobs: ${this.jobs.length+1}`)

      try{

        //load osm
        if(fs.existsSync(`${this.savePath}OSM_${job.x}_${job.y}.json`)){
          osm = JSON.parse(fs.readFileSync(`${this.savePath}OSM_${job.x}_${job.y}.json`)).features;
        } else{
          osm = await this.loadOSM(job);
        }

        treeOSM.load(this.reqArea(osm));

        //load dtm
        if(!fs.existsSync(`${this.savePath}DTM_${job.x}_${job.y}.json`)){
          await this.loadHttp(job, job.dtm, 'DTM', treeOSM);
        }

        if(!fs.existsSync(`${this.savePath}DSM_${job.x}_${job.y}.json`)){
          await this.loadHttp(job, job.dsm, 'DSM', treeOSM);
        }


      } catch (err){
        log.app(err)
      }

    }


  }

  loadOSM(job){

    const urlShort = job.osm.substr(0, 125)
    log.app(`${urlShort} Start Download`);

    return new Promise ((resolve) => {

      request.get(job.osm, (err, res, body) => {

        log.app(`${urlShort} download complete`);

        try{
          if (err) throw err;


          if(res.statusCode !== 200){
            throw `Source was not found: ${url}`;
          }

          log.app(`${urlShort} convert to Geojson and prepare data`);

          const osm = osmtogeojson(JSON.parse(body)).features.filter((feature)=>{
            return feature.geometry.type === 'Polygon';
          });



          this.write('OSM', [job.x, job.y], {
            "type": "FeatureCollection",
            "features": osm
          }, () => {
            log.app('success writing osm');

            resolve(osm);
          });

        }catch(err){
          log.app(err);
          resolve();
        }

      });

    });

  }

  loadHttp(job, surfacemodel, modelName,treeOSM){

    return new Promise( (resolve) => {

      const urlString = `${surfacemodel}${job.tileX/1000}_${job.tileY/1000}.zip`;

      log.app(`${urlString} Start Download`);

      request.get({url: urlString, encoding: null }, (err, res, body) => {

        try{

          if (err) throw err;

          if(res.statusCode !== 200){
            throw `Source was not found: ${urlString}`;
          }

          log.app(`${urlString} Download complete`);
          log.app(`${urlString} Start unzip`);

          let allPositions = this.unzip(body);

          log.app(`${urlString} unzip done `);

          allPositions = this.filterdata(allPositions, treeOSM);

          this.write(modelName, [job.x, job.y], allPositions, (res, err)=>{
            if(err){
              throw err;
            }
            resolve(allPositions);
          });

        } catch (err){
          log.app(err);
          resolve();
        }

      });

    });

  }

  write(type, tileNumber, data, callback){

    fs.writeFile(`${this.savePath}${type}_${tileNumber[0]}_${tileNumber[1]}.json`, JSON.stringify(data), 'utf8', (err) => {
      if (err){
        callback(err);
      }
      log.app(`${type}_${tileNumber[0]}_${tileNumber[1]} write to folder done`);
      callback(true);
    });

  }

  unzip(body){

    const zipEntries = new AdmZip(body).getEntries();
    const lines = zipEntries[0].getData().toString('utf8').split('\n');

    const position = [];
    lines.forEach( line =>{
      if (line === undefined) return;

      const val = line.split(' ');
      const x = parseInt(val[0]);
      const y = parseInt(val[1]);
      const z = parseFloat(val[2]);

      if(!isNaN(x) && !isNaN(y) && !isNaN(z)){

        const temp = proj4( util.UTMPROJBER, util.UTMPROJWORLD, [x, y]);
        position.push({x: parseInt(temp[0]), y: parseInt(temp[1]), z: z});

      }

    });

    return position;

  }

  reqArea(osm) {
  return osm.map( feature => {
    const boundsLatLng = Bounds.fromGeometry(feature.geometry);
    const boundsMin = proj4(util.LONGLATPROJ, util.UTMPROJWORLD, [boundsLatLng.min[0], boundsLatLng.min[1]]);
    const boundsMax = proj4(util.LONGLATPROJ, util.UTMPROJWORLD, [boundsLatLng.max[0], boundsLatLng.max[1]]);

    return {
      minX: parseInt(boundsMin[0]),
      minY: parseInt(boundsMin[1]),
      maxX: parseInt(boundsMax[0]),
      maxY: parseInt(boundsMax[1]),
      feature: feature
    };
  });
}

  filterdata(position, tree){

    return position.filter( item => {
      return tree.collides({minX: item.x, minY: item.y, maxX: item.x, maxY: item.y});
      });

  }

  round(num){
    return Math.round(num * 1000000) / 1000000;
  }

};




