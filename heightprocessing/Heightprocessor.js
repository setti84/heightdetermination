const fs = require('fs');
const RBush = require('rbush');
const Bounds = require('./../Bounds.js');
const proj4 = require('proj4');
const util = require('./../util.js');

const config = require('./../config.json');
const Logger = require('./../Logger.js');

const log = new Logger('./log/log.txt', true , config.logInFile);

module.exports = class Heightprocessor {

  constructor(getPath = './data/preprocessed/', savePath = './data/heightprocessed/', jobs = []){

    this.jobs = jobs;

    this.getPath = getPath;
    this.savePath = savePath;

    if (!fs.existsSync(this.getPath)){
      log.app(Error(`Could not find folder for heigthprocessing ${this.getPath}`));
    }

    if (!fs.existsSync(this.savePath)){
      fs.mkdirSync(this.savePath);
    }

  }

  getJobs(){

    return new Promise((resolve) =>{

      fs.readdir(this.getPath, (err, files) => {
        files.forEach( file => {
          if(file.substring(0,7) === 'diffDOM'){

            const filename = file.substring(0,file.length-5);
            const parts = filename.split("_");

            this.jobs.push({
              x: parts[1],
              y: parts[2],
              osm: `OSM_${parts[1]}_${parts[2]}.json`,
              diffDOM: file
            });

          }
        });
        log.app(`Jobs for Heightprocessing: ${this.jobs.length}`);
        resolve();
      });
    });

  }

  process(){

    while (this.jobs.length > 0) {

      const job = this.jobs.shift();

      log.app(`Job: ${job.x}_${job.y} start`);

      const diffDOMtree = new RBush(), pointsinPolygon = [], bounds = new Bounds(), tileOverlapp = 500;
      let allPoints;

      try{

        // read buildings
        const osm = JSON.parse(fs.readFileSync(this.getPath + job.osm, 'utf8'));
        // read allPoints for same area as buildings
        const pointsMainArea = JSON.parse(fs.readFileSync(this.getPath + job.diffDOM, 'utf8')).map( point => {

          bounds.extend([ point.x, point.y ]);
          return this.treePreperation(point);

        });

        bounds.min[0] = bounds.min[0]-tileOverlapp;
        bounds.min[1] = bounds.min[1]-tileOverlapp;
        bounds.max[0] = bounds.max[0]+tileOverlapp;
        bounds.max[1] = bounds.max[1]+tileOverlapp;

        log.app(`Job: ${job.x}_${job.y} Main area: ${pointsMainArea.length} Points loaded`);
        log.app(`Job: ${job.x}_${job.y} read main file done`);

        // get additional point data around tile border
        allPoints = [...pointsMainArea, ...this.loadAdditionalPointData(job, bounds)];

        log.app(`Job: ${job.x}_${job.y} load additional data files done`);
        log.app(`Job: ${job.x}_${job.y} All Points: ${allPoints.length}`);

        diffDOMtree.load(allPoints);

        osm.features.forEach( building => pointsinPolygon.push(this.getHeight(diffDOMtree, building)));

        fs.writeFileSync(`${this.savePath}OSM_height_${job.x}_${job.y}.geojson`, JSON.stringify({
          "type": "FeatureCollection",
          "features": pointsinPolygon
        }));

        log.app(`Job: ${job.x}_${job.y} write data done`);

      } catch(err){
        log.app(err);
        // this.deleteFiles(job);

      }

    }
  }

  getHeight(diffDOMpoints, building){

    function median(numbers) {
      const sorted = numbers.slice().sort((a, b) => a - b);
      const middle = Math.floor(sorted.length / 2);

      if (sorted.length % 2 === 0) {
        return (sorted[middle - 1] + sorted[middle]) / 2;
      }

      return parseInt(sorted[middle]);
    }

    const boundsLatLng = Bounds.fromGeometry(building.geometry);
    const boundsMin = proj4(util.LONGLATPROJ, util.UTMPROJWORLD, [boundsLatLng.min[0], boundsLatLng.min[1]]);
    const boundsMax = proj4(util.LONGLATPROJ, util.UTMPROJWORLD, [boundsLatLng.max[0], boundsLatLng.max[1]]);

    const heightPoints = diffDOMpoints.search({
      minX: parseInt(boundsMin[0]),
      minY: parseInt(boundsMin[1]),
      maxX: parseInt(boundsMax[0]),
      maxY: parseInt(boundsMax[1]),
    });

    const heightPointsBuilding = this.filterPoints(building.geometry, heightPoints).map( (point) =>{
      return parseFloat(point.feature.z);
    });

    const properties = {
      "heightMedian": parseInt(median(heightPointsBuilding)),
      "buildingType": building.properties.building,
      "height": building.properties.height,
      "minHeight": building.properties["min_height"],
      "roofShape": building.properties["roof:shape"],
      "buildingLevels": building.properties["building:levels"],
      "roofHeight": building.properties["roof:height"],
      "roofLevels": building.properties["roof:levels"],
    };

    return {
      "type": "Feature",
      "properties": properties,
      "geometry":  building.geometry
    };

  }

  filterPoints(buildingGeom, heightPoints){

    return heightPoints.filter( (point) =>{

      const temp =  proj4( util.UTMPROJWORLD, util.LONGLATPROJ, [point.feature.x, point.feature.y]);

      if(util.pointInPolygon(temp, buildingGeom.coordinates[0])) return true ;

    });

  }

  treePreperation(point){
    return {
      minX: point.x,
      minY: point.y,
      maxX: point.x,
      maxY: point.y,
      feature: {x: point.x, y: point.y, z: point.z}
    };
  }

  loadAdditionalPointData(job, bounds) {

    const x = parseInt(job.x), y = parseInt(job.y);

    const diffDOMs = [
      {x: x - 1, y: y - 1 }, {x: x    , y: y - 1 }, {x: x + 1, y: y - 1 },
      {x: x - 1, y: y}     , {x: x + 1, y: y},
      {x: x - 1, y: y + 1 }, {x: x ,    y: y + 1 }, {x: x + 1, y: y + 1 }
    ];

    // load diffDOMs around the OSM tile (but in area of bounds) and add to available points for lookup
    return diffDOMs.reduce( (result, tile) =>{

      try{

        // reads and filters each point in the extend of bounds from addtional data around main tile
        const filePoints = JSON.parse(fs.readFileSync(`${this.getPath}diffDOM_${tile.x}_${tile.y}.json`, 'utf8'))
          .reduce( ( filtered, element ) => {

            if(element.x > bounds.min[0] && element.x < bounds.max[0] &&
               element.y > bounds.min[1] && element.y < bounds.max[1]
            ){
              filtered.push(this.treePreperation(element));
            }

            return filtered;

        }, []);

        return [...result, ...filePoints];

      } catch(err){
        return result;
      }

    }, []);

  }

};
