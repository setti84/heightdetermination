const fs = require('fs');
const { exec } = require('child_process');
const proj4 = require('proj4');
const util = require('./../../util.js');


const createGeojson = (filedata) => {

  const features = [];
  filedata.forEach( e =>{

    const temp =  proj4( util.UTMPROJWORLD, util.LONGLATPROJ, [e.x, e.y]);

    features.push({
      "type": "Feature",
      "properties": {
        "z": parseInt(e.z)
      },
      "geometry": {
        "type": "Point",
        "coordinates":   [temp[0], temp[1]]
      }})
  });

  return JSON.stringify({
    "type": "FeatureCollection",
    "features": features
  });


};

const getJob = () => {

  const job = files.shift();

  const jobNumber = job.substring(0, job.indexOf("."));
  const position = jobNumber.split("_");



    console.log(`process job ${jobNumber}`);

    const filedata = JSON.parse(fs.readFileSync(job));

    fs.writeFileSync(`dDOM_${position[1]}_${position[2]}.geojson`, createGeojson(filedata));

    exec(`gdal_rasterize -of GTiff -a z -ot Byte -ts 1500 1500 dDOM_${position[1]}_${position[2]}.geojson dDOM_${position[1]}_${position[2]}.GTiff`, ()=>{

      if(files.length > 0){
            getJob();

       }

    });

    // fs.unlinkSync(`dDOM_${position[1]}_${position[2]}.geojson`);
    // exec(`gdal_rasterize -of GTiff -a z -ot Byte -a_nodata 0 -ts 1000 1000 temp.geojson testProjLatLon.GTiff`);
    // exec(`gdal_rasterize -of GTiff -a z -ot Byte -a_nodata 0 -tap temp.geojson testProjLatLon.GTiff`);
    // exec(`gdal_rasterize -of GTiff -a z -ot Byte -a_nodata 0 -tap temp.geojson testProjLatLon.GTiff`);
    // exec(`gdal_rasterize -of GTiff -a z -init 0 -ot Byte -ts 1500 1500 temp.geojson testProjLatLon2.GTiff`);


};


// ###########################################################


const files = [];
fs.readdirSync('./').forEach( (file) =>{
  if(file.includes('diffDOM')){
    files.push(file)
  }
});

getJob(files);

