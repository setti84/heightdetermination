const fs = require('fs');
const exec = require('child_process').exec;

const filenameGPKG = 'buildings.gpkg';

const getJob = () => {

  const job = files.shift();

  if(files.length+1 === count){
    console.log(`process first job ${job}`);

    exec(`ogr2ogr -f GPKG ${filenameGPKG} -nln buildings ${job}`, ()=>{

      if(files.length > 0){
        getJob();
      }

    });

  } else{

    exec(`ogr2ogr -append ${filenameGPKG} -nln buildings ${job}`, ()=>{
      console.log(`process ${job}`);
      if(files.length > 0){
        getJob();
      } else{
      //   last run
        exec(`ogr2ogr -f "ESRI Shapefile" allBuildings ${filenameGPKG} `, ()=> console.log('done writing shapefile'));

      }
    });

  }

}

const files = [];

fs.readdirSync('./').forEach( (file) =>{
  if(file.includes('.geojson')){
    files.push(file)
  }
});

const count = files.length;

getJob();

