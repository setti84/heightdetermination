module.exports = class Bounds {
  constructor (bounds = {}) {
    this.min = bounds.min || [Infinity, Infinity];
    this.max = bounds.max || [-Infinity, -Infinity];
  }

  extend (point) {
    if (point[0] < this.min[0]) this.min[0] = point[0];
    if (point[1] < this.min[1]) this.min[1] = point[1];
    if (point[0] > this.max[0]) this.max[0] = point[0];
    if (point[1] > this.max[1]) this.max[1] = point[1];
  }

  get () {
    return {min: this.min, max: this.max};
  }

  toString () {
    return [...this.min, ...this.max].join(',');
  }

  buffer (amount) {
    this.min[0] -= amount;
    this.min[1] -= amount;
    this.max[0] += amount;
    this.max[1] += amount;
  }

  getCenter () {
    return [
      (this.min[0] + this.max[0]) / 2,
      (this.min[1] + this.max[1]) / 2
    ];
  }

  static fromGeometry (geometry) {
    const
      type = geometry.type,
      coords = geometry.coordinates,
      bounds = new Bounds();

    if (type === 'Point') {
      bounds.extend(coords);
      return bounds;
    }

    if (type === 'LineString' || type === 'MultiPoint') {
      coords.forEach(point => {
        bounds.extend(point);
      });
      return bounds;
    }

    if (type === 'MultiLineString') {
      coords.forEach(line => {
        line.forEach(point => {
          bounds.extend(point);
        });
      });
      return bounds;
    }

    if (type === 'Polygon' && coords.length) {
      coords[0].forEach(point => {
        bounds.extend(point);
      });
      return bounds;
    }

    if (type === 'MultiPolygon') {
      coords.forEach(polygon => {
        if (polygon[0]) {
          polygon[0].forEach(point => {
            bounds.extend(point);
          });
        }
      });
      return bounds;
    }
  }
};
