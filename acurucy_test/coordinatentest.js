
const Logger = require('../Logger.js');
const log = new Logger('./log/', true, false);

const util = require('../util.js');
const proj4 = require('proj4');


// Genuigkeitstest

// siehe PDF
const eingang = [388573.779 , 5822950.794 ];


log.app(`Eingangskoordinate UTM Zone 33N ETRS 89: ${eingang}`);


const temp = proj4(util.UTMPROJBER, util.LONGLATPROJ, eingang);
const temp2 = proj4('+proj=utm +zone=33 +ellps=GRS80 +units=m +no_defs ', util.LONGLATPROJ, eingang);


const chain = proj4(util.UTMPROJBER, util.UTMPROJWORLD, eingang);
const chain2 = proj4(util.UTMPROJBER, util.LONGLATPROJ, chain);
const chain3 = proj4(util.UTMPROJBER, util.LONGLATPROJ, eingang);



log.app(`in WGS84:                      ${temp}`);
log.app(proj4( util.LONGLATPROJ,util.UTMPROJBER, temp));
